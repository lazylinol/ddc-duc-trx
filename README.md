# About

## History 
Project is based on ideas and solutions from EU1SW initially published on https://tinyurl.com/yyz3j3ba with various modifications.

## Description
Currently project contains schematics, and PCB files + Gerber files for so-called mainboard.

## Tools
To view/modify schematics and PCB files DipTrace CAD - https://diptrace.com/ is needed.
