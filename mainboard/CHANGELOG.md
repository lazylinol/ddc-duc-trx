# Changelog

## Version 0.2
* codec position fixed, moved 1.5 millimeters inside, away from board border
* removed C42 capacitor, following numeration was shifted accordingly

## Version 0.1
Initial release. This version contains few bugs:
* mounted codec PCB exceeds few millimeters of mainboard border
* one of C15/C42 capacitors is not necessary, and must be replaced with 0 Ohm resistor during assembly
